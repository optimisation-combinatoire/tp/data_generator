package lib

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"time"
)

type MinizincSchedulingData struct {
	NB_PATIENTS int
	LAST_PERIOD int

	MinimumScore MinimumScore
	Patients     Patients
	NbUnit       NbUnit
	Duration     Duration
	WaitingTime  WaitingTime
	Penalty      Penalty
}

func NewData(info *InstancesStatistic) MinizincSchedulingData {
	return MinizincSchedulingData{
		NB_PATIENTS:  info.NumberPatient,
		LAST_PERIOD:  info.LastPeriod,
		MinimumScore: minimumScore,
		Patients:     generateRandomPatient(info.MeanPatient, info.VariancePatient, info.NumberPatient, info.LastPeriod, info.MeanArrival, info.VarianceArrival),
		NbUnit:       nbUnit,
		Duration:     duration,
		WaitingTime:  waitingTime,
		Penalty:      penalty,
	}
}
func (m MinizincSchedulingData) File() error {

	f, err := os.Create("./data_generated.dzn")
	if err != nil {
		return err
	}
	_, err = f.WriteString(m.String())
	if err != nil {
		f.Close()
		return err
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}
func (m MinizincSchedulingData) String() string {
	resp := fmt.Sprintf(`
	NB_PATIENTS = %v;
	LAST_PERIOD = %v;`, m.NB_PATIENTS, m.LAST_PERIOD)
	dataElement := []string{
		m.MinimumScore.String(),
		m.Patients.String(),
		m.NbUnit.String(),
		m.Duration.String(),
		m.WaitingTime.String(),
		m.Penalty.String(),
	}
	for _, el := range dataElement {
		resp += fmt.Sprintf("\n%v", el)

	}

	return resp
}
func generateRandomPatient(meanPatient int, variancePatient int, numberPatient int, lastPeriod int, meanArrival int, varianceArrival int) Patients {
	ajustementTime := 3
	rand.Seed(time.Now().Unix())
	patientScore := make([]int, 0, numberPatient)
	patientArrival := make([]int, 0, lastPeriod)
	for i := 0; i < numberPatient; i++ {
		score := int(math.Abs(rand.NormFloat64()*float64(variancePatient) + float64(meanPatient)))
		if score > minimumScore.MINIMUM_SCORE_EXPECTANT {
			score = minimumScore.MINIMUM_SCORE_EXPECTANT
		}
		if score <= 0 {
			score = 1
		}
		patientScore = append(patientScore, score)

		arrival := int(math.Abs(rand.NormFloat64()*float64(varianceArrival) + float64(meanArrival)))
		if arrival > lastPeriod-ajustementTime {
			arrival = lastPeriod - ajustementTime
		} else if arrival == 0 {
			arrival = 1
		}

		patientArrival = append(patientArrival, arrival)
	}
	return Patients{
		PATIENT_ARRIVAL:        patientArrival,
		PATIENT_CATEGORY_SCORE: patientScore,
	}
}

type MinimumScore struct {
	MINIMUM_SCORE_EXPECTANT int
	MINIMUM_SCORE_IMMEDIATE int
	MINIMUM_SCORE_DELAYED   int
	MINIMUM_SCORE_MINIMAL   int
}

func (m MinimumScore) String() string {
	return fmt.Sprintf(`
	MINIMUM_SCORE_EXPECTANT = %v;
	MINIMUM_SCORE_IMMEDIATE = %v;
	MINIMUM_SCORE_DELAYED = %v;
	MINIMUM_SCORE_MINIMAL = %v;
	`, m.MINIMUM_SCORE_EXPECTANT, m.MINIMUM_SCORE_IMMEDIATE, m.MINIMUM_SCORE_DELAYED, m.MINIMUM_SCORE_MINIMAL)

}

type Penalty struct {
	PENALTY_IMMDEDIATE_WAITING_TIME_TOO_LONG int
	PENALTY_DELAYED_WAITING_TIME_TOO_LONG    int
	PENALTY_MINIMAL_WAITING_TIME_TOO_LONG    int
	PENALTY_EXPECTANT_WAITING_TIME_TOO_LONG  int
	PENALTY_IMMDEDIATE_MISSPLACED            int
	PENALTY_DELAYED_MISSPLACED               int
	PENALTY_MINIMAL_MISSPLACED               int
	PENALTY_EXPECTANT_MISSPLACED             int
	MAX_PENALTY                              int
}

func (p Penalty) String() string {
	return fmt.Sprintf(`
	PENALTY_IMMDEDIATE_WAITING_TIME_TOO_LONG = %v;
	PENALTY_DELAYED_WAITING_TIME_TOO_LONG = %v;
	PENALTY_MINIMAL_WAITING_TIME_TOO_LONG = %v;
	PENALTY_EXPECTANT_WAITING_TIME_TOO_LONG = %v;
	PENALTY_IMMDEDIATE_MISSPLACED = %v;
	PENALTY_DELAYED_MISSPLACED = %v;
	PENALTY_MINIMAL_MISSPLACED = %v;
	PENALTY_EXPECTANT_MISSPLACED = %v;
	MAX_PENALTY = %v;
		`, p.PENALTY_IMMDEDIATE_WAITING_TIME_TOO_LONG,
		p.PENALTY_DELAYED_WAITING_TIME_TOO_LONG,
		p.PENALTY_MINIMAL_WAITING_TIME_TOO_LONG,
		p.PENALTY_EXPECTANT_WAITING_TIME_TOO_LONG,
		p.PENALTY_IMMDEDIATE_MISSPLACED,
		p.PENALTY_DELAYED_MISSPLACED,
		p.PENALTY_MINIMAL_MISSPLACED,
		p.PENALTY_EXPECTANT_MISSPLACED,
		p.MAX_PENALTY)
}

type WaitingTime struct {
	WAITING_TIME_IMMEDIATE int
	WAITING_TIME_DELAYED   int
	WAITING_TIME_MINIMAL   int
	WAITING_TIME_EXPECTANT int
}

func (w WaitingTime) String() string {
	return fmt.Sprintf(`
	WAITING_TIME_IMMEDIATE = %v;
	WAITING_TIME_DELAYED = %v;
	WAITING_TIME_MINIMAL = %v;
	WAITING_TIME_EXPECTANT = %v;
	`, w.WAITING_TIME_IMMEDIATE,
		w.WAITING_TIME_DELAYED,
		w.WAITING_TIME_MINIMAL,
		w.WAITING_TIME_EXPECTANT)
}

type Duration struct {
	DURATION_RESUSCUTATION int
	DURATION_FIRST_AID     int
}

func (d Duration) String() string {
	return fmt.Sprintf(`
	DURATION_RESUSCUTATION = %v;
	DURATION_FIRST_AID = %v;
	`, d.DURATION_RESUSCUTATION, d.DURATION_FIRST_AID)
}

type NbUnit struct {
	NB_FIRST_AID_UNIT     int
	NB_RESUSCUTATION_UNIT int
}

func (n NbUnit) String() string {
	return fmt.Sprintf(`
	NB_FIRST_AID_UNIT = %v;
	NB_RESUSCITATION_UNIT = %v;
	`, n.NB_FIRST_AID_UNIT, n.NB_RESUSCUTATION_UNIT)
}

type Patients struct {
	PATIENT_ARRIVAL        []int
	PATIENT_CATEGORY_SCORE []int
}

func (p Patients) String() string {
	patientArrival := "["
	for _, el := range p.PATIENT_ARRIVAL {
		patientArrival += fmt.Sprintf("%d,", el)
	}
	patientArrival = patientArrival[:len(patientArrival)-1]
	patientArrival += "]"

	patientCategoryScore := "["
	for _, el := range p.PATIENT_CATEGORY_SCORE {
		patientCategoryScore += fmt.Sprintf("%d,", el)
	}
	patientCategoryScore = patientCategoryScore[:len(patientCategoryScore)-1]

	patientCategoryScore += "]"
	return fmt.Sprintf(`
	PATIENT_ARRIVAL = %v;
	PATIENT_CATEGORY_SCORE = %v;
	`, patientArrival, patientCategoryScore)
}

var minimumScore = MinimumScore{
	MINIMUM_SCORE_EXPECTANT: 1100,
	MINIMUM_SCORE_IMMEDIATE: 800,
	MINIMUM_SCORE_DELAYED:   500,
	MINIMUM_SCORE_MINIMAL:   200,
}

var nbUnit = NbUnit{
	NB_FIRST_AID_UNIT:     5,
	NB_RESUSCUTATION_UNIT: 3,
}
var duration = Duration{
	DURATION_RESUSCUTATION: 1,
	DURATION_FIRST_AID:     1,
}

var waitingTime = WaitingTime{
	WAITING_TIME_IMMEDIATE: 1,
	WAITING_TIME_DELAYED:   3,
	WAITING_TIME_MINIMAL:   10,
	WAITING_TIME_EXPECTANT: 40,
}

var penalty = Penalty{
	PENALTY_IMMDEDIATE_WAITING_TIME_TOO_LONG: 10,
	PENALTY_DELAYED_WAITING_TIME_TOO_LONG:    9,
	PENALTY_MINIMAL_WAITING_TIME_TOO_LONG:    2,
	PENALTY_EXPECTANT_WAITING_TIME_TOO_LONG:  7,

	PENALTY_IMMDEDIATE_MISSPLACED: 5,
	PENALTY_DELAYED_MISSPLACED:    4,
	PENALTY_MINIMAL_MISSPLACED:    2,
	PENALTY_EXPECTANT_MISSPLACED:  4,
	MAX_PENALTY:                   10,
}
