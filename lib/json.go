package lib

import (
	"encoding/json"
	"os"
)

type Json struct {
	Data                      []Data   `json:"data"`
	Penalty                   int      `json:"penalty"`
	NumberOfRessucitationUnit int      `json:"number_of_ressucitation_unit"`
	NumberOfFirstAidUnit      int      `json:"number_of_first_aid_unit"`
	PatientCareStart          []int    `json:"patient_care_start"`
	PatientCareEnd            []int    `json:"patient_care_end"`
	PatientLocation           []string `json:"patient_location"`
}
type Data struct {
	Start    int    `json:"start"`
	End      int    `json:"end"`
	Location string `json:"location"`
	Penality int    `json:"penality"`
	Category string `json:"category"`
	Score    int    `json:"score"`
}

func (j Json) File(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	byteJson, err := json.Marshal(j)
	if err != nil {
		return err
	}
	stringJson := string(byteJson)
	_, err = f.WriteString(stringJson)
	if err != nil {
		f.Close()
		return err
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil

}
