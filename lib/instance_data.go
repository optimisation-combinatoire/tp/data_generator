package lib

type InstancesStatistic struct {
	MeanPatient       int
	VariancePatient   int
	NumberPatient     int
	LastPeriod        int
	MeanArrival       int
	VarianceArrival   int
	NumberNewPatients int
}
