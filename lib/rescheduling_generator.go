package lib

import (
	"fmt"
	"os"
)

type MinizincReschedulingData struct {
	MinizincSchedulingData
	NB_PATIENTS_NEW_PATIENT int
	OldPatient              OldPatient
	NewPatient              NewPatient
}

func (m MinizincReschedulingData) String() string {
	resp := fmt.Sprintf(`
	NB_PATIENTS_NEW_PATIENT = %v;
	NB_PATIENTS = %v;
	LAST_PERIOD = %v;`, m.NB_PATIENTS_NEW_PATIENT, m.NB_PATIENTS, m.LAST_PERIOD)

	dataElement := []string{
		m.MinimumScore.String(),
		m.Patients.String(),
		m.NbUnit.String(),
		m.Duration.String(),
		m.WaitingTime.String(),
		m.Penalty.String(),
		m.OldPatient.String(),
		m.NewPatient.String(),
	}

	for _, el := range dataElement {
		resp += fmt.Sprintf("\n%v", el)

	}
	return resp
}
func (m MinizincReschedulingData) File() error {

	f, err := os.Create("./data_generated_reschedule.dzn")
	if err != nil {
		return err
	}
	_, err = f.WriteString(m.String())
	if err != nil {
		f.Close()
		return err
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func NewReschedulingData(schedulingData MinizincSchedulingData, jsonOldPatient *Json, info *InstancesStatistic) MinizincReschedulingData {

	oldPatient := OldPatient{
		PATIENT_CARE_START: jsonOldPatient.PatientCareStart,
		PATIENT_CARE_END:   jsonOldPatient.PatientCareEnd,
		PATIENT_LOCATION:   jsonOldPatient.PatientLocation,
	}
	schedulingData.LAST_PERIOD += schedulingData.LAST_PERIOD
	return MinizincReschedulingData{
		MinizincSchedulingData:  schedulingData,
		NB_PATIENTS_NEW_PATIENT: info.NumberNewPatients,
		OldPatient:              oldPatient,
		NewPatient:              generateNewRandomPatient(info.MeanPatient, info.VariancePatient, info.NumberNewPatients, info.LastPeriod, info.LastPeriod, info.VarianceArrival),
	}
}
func generateNewRandomPatient(meanPatient int, variancePatient int, numberPatient int, lastPeriod int, meanArrival int, varianceArrival int) NewPatient {
	patients := generateRandomPatient(meanPatient, variancePatient, numberPatient, lastPeriod, meanArrival, varianceArrival)
	return NewPatient{
		Patients: patients,
	}
}

type OldPatient struct {
	PATIENT_CARE_START []int
	PATIENT_CARE_END   []int
	PATIENT_LOCATION   []string
}

func (p OldPatient) String() string {
	patientArrival := "["
	for _, el := range p.PATIENT_CARE_START {
		patientArrival += fmt.Sprintf("%d,", el)
	}
	patientArrival = patientArrival[:len(patientArrival)-1]
	patientArrival += "]"

	patientCategoryScore := "["
	for _, el := range p.PATIENT_CARE_END {
		patientCategoryScore += fmt.Sprintf("%d,", el)
	}
	patientCategoryScore = patientCategoryScore[:len(patientCategoryScore)-1]
	patientCategoryScore += "]"

	patientLocation := "["
	for _, el := range p.PATIENT_LOCATION {
		patientLocation += fmt.Sprintf("%v,", el)
	}
	patientLocation = patientLocation[:len(patientLocation)-1]
	patientLocation += "]"

	return fmt.Sprintf(`
	PATIENT_CARE_START = %v;
	PATIENT_CARE_END = %v;
	PATIENT_LOCATION = %v;
	`, patientArrival, patientCategoryScore, patientLocation)
}

type NewPatient struct {
	Patients
}

func (p NewPatient) String() string {
	patientArrival := "["
	for _, el := range p.PATIENT_ARRIVAL {
		patientArrival += fmt.Sprintf("%d,", el)
	}
	patientArrival = patientArrival[:len(patientArrival)-1]
	patientArrival += "]"

	patientCategoryScore := "["
	for _, el := range p.PATIENT_CATEGORY_SCORE {
		patientCategoryScore += fmt.Sprintf("%d,", el)
	}
	patientCategoryScore = patientCategoryScore[:len(patientCategoryScore)-1]

	patientCategoryScore += "]"
	return fmt.Sprintf(`
	NEW_PATIENT_ARRIVAL = %v;
	NEW_PATIENT_CATEGORY_SCORE = %v;
	`, patientArrival, patientCategoryScore)
}
