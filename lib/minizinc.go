package lib

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os/exec"
	"regexp"
)

const TIME_SOLVER int = 100000

func RunMinizinc(modelFile string) (Json, error) {
	cmd := exec.Command("minizinc",
		modelFile,
		"--solver", "chuffed",
		"--search-complete-msg", "",
		"--soln-sep", "!",
		"-a",
		"--solver-time-limit", fmt.Sprintf("%v", TIME_SOLVER),
		"--unknown-msg", "")
	var stderr bytes.Buffer
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr

	err := cmd.Run()

	if err != nil {
		fmt.Println(stderr.String())
		fmt.Println(out.String())
		return Json{}, err
	}
	jsonResponse := out.String()
	re := regexp.MustCompile("!")
	indexes := re.FindAllIndex([]byte(jsonResponse), -1)
	lastSolutionIndex := indexes[len(indexes)-2][0]
	jsonResponse = jsonResponse[lastSolutionIndex+1 : len(jsonResponse)-2]
	var result Json
	err = json.Unmarshal([]byte(jsonResponse), &result)
	if err != nil {
		fmt.Println(jsonResponse)
		return Json{}, err
	}

	return result, nil
}
