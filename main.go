package main

import (
	"flag"
	"fmt"
	"os"
	"reflect"
	"time"

	"gitlab.com/constraintAutomaton/data_generator/lib"
)

var DEFAULT_MEAN_PATIENT int = 100
var DEFAULT_VARIANCE_PATIENT int = 150
var DEFAULT_NUMBER_PATIENT int = 20
var DEFAULT_LAST_PERIOD int = 50
var DEFAULT_MEAN_ARRIVAL int = 25
var DEFAULT_VARIANCE_ARRIVAL int = 10
var DEFAULT_NUMBER_NEW_PATIENTS int = 10

func main() {
	var meanPatient int
	var variancePatient int
	var numberPatient int
	var lastPeriod int
	var meanArrival int
	var varianceArrival int
	var numberNewPatients int

	flag.IntVar(&meanPatient, "mp", DEFAULT_MEAN_PATIENT, "means patients category score")
	flag.IntVar(&variancePatient, "vp", DEFAULT_VARIANCE_PATIENT, "variance patients category score")
	flag.IntVar(&numberPatient, "np", DEFAULT_NUMBER_PATIENT, "number of patients")
	flag.IntVar(&lastPeriod, "l", DEFAULT_LAST_PERIOD, "last period")
	flag.IntVar(&meanArrival, "ma", DEFAULT_MEAN_ARRIVAL, "mean of the arrival of patients")
	flag.IntVar(&varianceArrival, "va", DEFAULT_VARIANCE_ARRIVAL, "variance of the arrival of patients")
	flag.IntVar(&numberNewPatients, "nnp", DEFAULT_NUMBER_NEW_PATIENTS, "number of patient for the rescheduling")

	flag.Parse()
	instanceStatistic := lib.InstancesStatistic{
		MeanPatient:       meanPatient,
		VariancePatient:   variancePatient,
		NumberPatient:     numberPatient,
		LastPeriod:        lastPeriod,
		MeanArrival:       meanArrival,
		VarianceArrival:   varianceArrival,
		NumberNewPatients: numberNewPatients,
	}
	schedulingData := lib.NewData(&instanceStatistic)

	fmt.Printf(`Data generation with
	patient mean : %v
	patient variance : %v
	number of patient : %v
	last period : %v
	mean arrivial : %v
	variance arrival : %v
	number of new patient : %v

	
`, meanPatient, variancePatient, numberPatient, lastPeriod, meanArrival, varianceArrival, numberNewPatients)

	if err := schedulingData.File(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	startScheduling := time.Now()
	schedulingResult, err := lib.RunMinizinc("./scheduling.mzn")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	endScheduling := time.Now()
	executionTimeScheduling := endScheduling.Sub(startScheduling)
	fmt.Printf("Scheduling took %v s\n\n", executionTimeScheduling.Seconds())

	if err := schedulingResult.File("./result_schedule.json"); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	reschedulingData := lib.NewReschedulingData(schedulingData, &schedulingResult, &instanceStatistic)
	if err := reschedulingData.File(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	startRecheduling := time.Now()
	reScheduleResult, err := lib.RunMinizinc("./rescheduling.mzn")
	if err != nil {
		fmt.Println(err)
		fmt.Println("Failed at running minizinc for rescheduling")
		os.Exit(1)
	}
	endRescheduling := time.Now()
	executionTimeRescheduling := endRescheduling.Sub(startRecheduling)
	fmt.Printf("Rescheduling took %v s\n\n", executionTimeRescheduling.Seconds())
	if err := reScheduleResult.File("./result_reschedule.json"); err != nil {
		fmt.Println(err)
		fmt.Println("Failed at parse data into file for reschedule")
		os.Exit(1)
	}
	fmt.Println("latex report result: ")
	fmt.Printf(`
\hline
%v & %v & %v & %v & %v & %v & %v & %v & %v & %v & %v & %v & %v \\
\hline
	\n`,
		numberPatient, lastPeriod, meanArrival, varianceArrival, meanPatient, variancePatient, numberNewPatients,
		executionTimeScheduling.Seconds(), executionTimeRescheduling.Seconds(),
		!(executionTimeScheduling.Milliseconds() >= int64(lib.TIME_SOLVER)), !(executionTimeScheduling.Milliseconds() >= int64(lib.TIME_SOLVER)), schedulingResult.Penalty, reScheduleResult.Penalty)
	if reflect.DeepEqual(reScheduleResult, lib.Json{}) {
		fmt.Println("Was not able to solve")
	}
}
